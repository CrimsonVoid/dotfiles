```bash
git clone --bare git@github.com:CrimsonVoid/dotfiles.git ${HOME}/.dotfiles
alias cfg='git --git-dir=${HOME}/.dotfiles/ --work-tree=${HOME}'

cfg checkout
cfg config --local status.showUntrackedFiles no
```
