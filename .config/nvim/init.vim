" Section Plugins {{{
call plug#begin('~/.local/share/nvim/plugged')

" General
Plug 'airblade/vim-gitgutter'
Plug 'godlygeek/tabular'
Plug 'honza/vim-snippets'
Plug 'kien/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/syntastic'
Plug 'sheerun/vim-polyglot'
Plug 'SirVer/ultisnips'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-sensible'
Plug 'Valloric/YouCompleteMe'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Yggdroot/indentLine'

" Language
Plug 'rust-lang/rust.vim'
Plug 'fatih/vim-go'
Plug 'davidhalter/jedi-vim'

" Colors
Plug 'w0ng/vim-hybrid'

call plug#end()
" }}}

" Vim Settings
syntax on
set t_Co=256
set colorcolumn=80,100
set laststatus=2
set noshowmode
set encoding=utf-8
set number
set ignorecase
set fillchars=vert:\│
set tabstop=4 expandtab shiftwidth=4 softtabstop=4
filetype plugin indent on
colorscheme hybrid
set background=dark
set showcmd
set cursorline
set lazyredraw

" move vertically by visual line
nnoremap j gj
nnoremap k gk

" Remap leader
let mapleader=','

" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>

" Tabs
nnoremap <Tab> gt
nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

" Clean search (highlight)
nnoremap <silent> <leader><space> :noh<cr>

" Switching windows
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h

" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

" Airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" Use ag for ctrlp
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'

" YCM - Ultisnips
let g:UltiSnipsExpandTrigger = "<c-e>"
let g:UltiSnipsJumpForwardTrigger = "<c-e><c-n>"
let g:UltiSnipsJumpBackwardTrigger = "<c-e><c-p>"
 
" Go
let g:syntastic_go_checkers = ['golint', 'govet', 'errcheckt']
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go'] }
let g:go_fmt_command = "goimports"

augroup configgroup
  autocmd!

  autocmd BufRead,BufNewFile *.sls setfiletype yaml
  autocmd BufRead,BufNewFile *.rs set colorcolumn=99

  autocmd FileType go nmap <Leader>dd <Plug>(go-def-vertical)
  autocmd FileType go nmap <Leader>dv <Plug>(go-doc-vertical)
  autocmd FileType go nmap <Leader>ge <Plug>(go-rename)
  autocmd FileType go nmap <Leader>gi <Plug>(go-info)
  autocmd FileType go nmap <leader>gr <Plug>(go-run)
  autocmd FileType go nmap <leader>gb <Plug>(go-build)
  autocmd FileType go nmap <leader>gt <Plug>(go-test)

  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
      \ formatoptions+=croq softtabstop=4 smartindent
      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

" vim:foldmethod=marker:foldlevel=0
