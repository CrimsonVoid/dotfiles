# Lines configured by zsh-newuser-install
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory extendedglob nomatch notify
unsetopt autocd beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename "/home/${USER}/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall

export TERM=screen-256color
prompt="%{%F{green}%}%n@%M%{%f%} [%{%F{blue}%}%~%{%f%}]%{%F{cyan}%}%#%{%f%} "
source ${HOME}/.config/user-dirs.dirs
export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"

# Respect XDG base dir spec
export LESSHISTFILE="${XDG_CONFIG_HOME}/less/history"
export GNUPGHOME="${XDG_CONFIG_HOME}/gnupg"
export HISTFILE="${XDG_CONFIG_HOME}/zsh/history"
export WEECHAT_HOME="${XDG_CONFIG_HOME}/weechat"
export VIMPERATOR_INIT=":source ${XDG_CONFIG_HOME}/vimperator/vimperatorrc"
export VIMPERATOR_RUNTIME="${XDG_CONFIG_HOME}/vimperator"

export RUSTUP_HOME="${XDG_DATA_HOME}/rustup"
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export STACK_ROOT="${XDG_DATA_HOME}/stack"
export LEIN_HOME="${XDG_DATA_HOME}/lein"
export BOOT_HOME="${XDG_DATA_HOME}/boot"

alias tmux='tmux -f ${XDG_CONFIG_HOME}/tmux/config'
alias wget='wget --hsts-file=${XDG_CONFIG_HOME}/wget/hsts'
alias weechat='weechat --dir ${XDG_CONFIG_HOME}/weechat'
alias ncmpcpp='ncmpcpp --config ${XDG_CONFIG_HOME}/ncmpcpp/config'

##########
## Rice
##########
alias cfg='git --git-dir=${HOME}/.dotfiles/ --work-tree=${HOME}'
export PATH=${PATH}:${HOME}/.local/bin

## Virtualenv Wrapper
# source ${HOME}/.local/bin/virtualenvwrapper.sh
# export WORKON_HOME=${HOME}/code/python/venv
# export PROJECT_HOME=${HOME}/code/python

## Rust
# source ${CARGO_HOME}/env
# export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HOME}/.local/lib
# export RUST_SRC_PATH=$(rustc --print sysroot)/lib/rustlib/src/rust/src

## Go
export GOPATH=${HOME}/code/go
export GOROOT=${HOME}/.local/opt/go
export PATH=${PATH}:${GOROOT}/bin:${GOPATH}/bin

##########
## Aliases
##########
pkg() {
    typeset -A pkg_cmd
    os=$(python -c "import platform; print(platform.linux_distribution()[0])")

    if [[ "${os}" == "debian" || "${os}" == "Ubuntu" ]]; then
        pkg_cmd=( \
            update  'sudo apt update && sudo apt upgrade' \
            install 'sudo apt install'   \
            remove  'sudo apt remove'     \
            search  'apt search'         \
            info    'apt show'           \
            hist    'cat /var/log/dpkg.log'   \
            clean   "sudo apt autoremove; \
                     sudo aptitude autoclean; \
                     sudo aptitude clean"     \
        )
    elif [[ "${os}" == "Fedora" ]]; then
        pkg_cmd=( \
            update  'sudo dnf upgrade'   \
            install 'sudo dnf install'   \
            remove  'sudo dnf remove'    \
            search  'dnf search'         \
            info    'dnf info'           \
            hist    'sudo dnf history'   \
            clean   'sudo dnf clean all' \
        )
    elif [[ "${os}" == "arch" ]]; then
        pkg_cmd=( \
            update  'sudo pacman -Syyu'    \
            install 'sudo pacman -S'       \
            remove  'sudo pacman -Rs'      \
            search  'pacman -Ss'           \
            info    'pacman -Si'           \
            hist    'echo "not supported"' \
            clean   'sudo pacman -Sc'      \
        )
    else
        echo "${os} not supported :("
        return
    fi

    case "${1}" in
        update | up | u)
            eval ${pkg_cmd[update]} $argv[2,-1];;
        install | in | i)
            eval ${pkg_cmd[install]} $argv[2,-1];;
        remove | rm | r)
            eval ${pkg_cmd[remove]} $argv[2,-1];;
        search | s)
            eval ${pkg_cmd[search]} $argv[2,-1];;
        info)
            eval ${pkg_cmd[info]} $argv[2,-1];;
        history | h)
            eval ${pkg_cmd[hist]} $argv[2,-1];;
        clean | cl | c)
            eval ${pkg_cmd[clean]} $argv[2,-1];;
        *)
            echo 'Usage:'
            echo '    update|up|u  - update your system'
            echo '    install|in|i - install selected packages'
            echo '    remove|rm|r  - remove packages'
            echo '    search|s     - search for packages'
            echo '    info         - get package info'
            echo '    history|h    - get package manager history'
            echo '    clean|cl|c   - clean up'
    esac
}

alias pup='pkg update'
alias pin='pkg install'
alias psr='pkg search'

## File
alias ls='ls --color --group-directories-first'
alias la='ls -A'
alias ll='ls -lh'
alias lh='ls -lAh'
alias mkdir='mkdir -p'
alias mv='mv -i'
alias cp='cp --reflink=auto -i'

alias own="sudo chown ${USER}:${USER}"
alias grep='egrep --color=auto --line-number'
alias rld='source ${XDG_CONFIG_HOME}/zsh/.zshrc'

## Dev
# alias vim='nvim'
alias vin='vim +NERDTree'
alias cdgo="cd ${GOPATH}/src/github.com/CrimsonVoid"
cppe () {
    clang++ \
    -std=c++11 \
    -Weverything \
    -Wno-c++98-compat \
    -Wno-c++98-compat-pedantic \
    "${1}.cpp" -o "${1}"
}

## Disk
alias df='df -T -h'
alias du='du -hc'
alias du1='du --max-depth=1 | sort -h'

## Net
alias ports='ss -napr'


##########
## Functions
##########

## Easy extract
extract () {
    arg=$ARGC

    if [[ -f $argv[$arg] ]]; then
        dir=$PWD
    elif [[ ! -d $argc[$arg] ]]; then
        echo "$argv[$arg] does not exist. Creating"
        dir=$argv[$arg]
        mkdir "$dir"
        arg=`expr $arg - 1`
    elif [[ -d $argv[$arg] ]]; then
        if [[ $argv[$arg] = '.' ]]; then
            dir=$PWD
            arg=`expr $arg - 1`
        elif [[ $argv[$arg] = '..' ]]; then
            # TODO - Does not work
            dir=${dirname $PWD}
            arg=`expr $arg - 1`
        else
            dir=$argv[$arg]
            arg=`expr $arg - 1`
        fi
    fi

    for i in {1..$arg}; do
        if [[ -f $argv[$i] ]]; then
            # TODO - Case insensitive extenstion matching
            case $argv[$i] in
                *.tar.bz2)  tar xvjf $argv[$i] -C "$dir" ;;
                *.tar.gz)   tar xvzf $argv[$i] -C "$dir" ;;
                *.bz2)      bunzip2 $argv[$i]            ;;
                *.rar)      unrar x $argv[$i] "$dir"     ;;
                *.gz)       gunzip $argv[$i]             ;;
                *.tar)      tar xvf $argv[$i] -C "$dir"  ;;
                *.tbz2)     tar xvjf $argv[$i] -C "$dir" ;;
                *.tgz)      tar xvzf $argv[$i] -C "$dir" ;;
                *.zip)      unzip $argv[$i] -d "$dir"    ;;
                *.Z)        uncompress $argv[$i]         ;;
                *.7z)       7za e $argv[$i] -o "$dir"    ;;
                *)          echo "Don't know how to extract '$i'" ;;
            esac
        else
            echo "'$i' is not a valid file!"
        fi
    done
}

## Creates an archive from given directory
mktar() { tar cvf  "${1%%/}.tar"     "${1%%/}/"; }
mktgz() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }
mktxz() { tar cvJf "${1%%/}.tar.xz"  "${1%%/}/"; }
mktbz() { tar cvjf "${1%%/}.tar.bz2" "${1%%/}/"; }

## Curl Dictionary Lookup
dict() { curl "dict://dict.org/d:${1%%/}" }

## Download and extract audio from YouTube video
ytmusic() {
    youtube-dl \
    --extract-audio \
    --audio-format mp3 \
    https://www.youtube.com/watch\?v=${1};
}
